module.exports = {
  apiBasePath: "http://3.1.115.199:8081",
  authPath: "/authorization/auth",
  authPathSignIn: "/SignIn",
  authPathSignOut: "/SignOut",
  authPathChangePassword: "/ChangePassword",
  authPathForgotPassword: "/ForgotPassword",
  authPathConfirmForgotPassword: "/ConfirmForgotPassword",
  authPathResendConfirmationCode: "/ResendConfirmationCode",
  authPathSignUp: "/SignUp",
  authPathSignUpConfirmation: "/SignUpConfirmation",
  authPathGetUserByToken: "/GetUserByToken",
  authPathUpdateUser: "/UpdateUser",

  eventPath: "/event",
  eventPathGetAllEvents: "/getListEvents",
};
