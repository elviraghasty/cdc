import React /*, { useContext }*/ from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  //Redirect,
} from "react-router-dom";

import HomePage from "./pages/HomePage";
import SignIn from "./pages/authentication/SignIn";
import SignUp from "./pages/authentication/SignUp";
import ChangePassword from "./pages/authentication/ChangePassword";
import ForgotPassword from "./pages/authentication/ForgotPassword";
import SignUpConfirmation from "./pages/authentication/SignUpConfirmation";
import ConfirmForgotPassword from "./pages/authentication/ConfirmForgotPassword";
import Layout from "./layout/Layout";
//import { AppStore } from "./stores/AppStore";

const AuthPage = () => {
  //const { state, dispatch } = useContext(AppStore);
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/auth/signup" component={SignUp} />
          <Route path="/auth/forgotpassword" component={ForgotPassword} />
          <Route
            path="/auth/confirmforgotpassword"
            component={ConfirmForgotPassword}
          />
          <Route path="/auth/changepassword" component={ChangePassword} />
          <Route
            path="/auth/signupconfirmation"
            component={SignUpConfirmation}
          />
          <Route path="/home" component={HomePage} />
          <Route path="/" component={SignIn} />

          {/* <Route
            exact
            path="/"
            render={() => {
              return state.isAuthenticated ? (
                <Redirect to="/home" />
              ) : (
                <Redirect to="/auth/signin" />
              );
            }}
          /> */}
        </Switch>
      </div>
    </Router>
  );
};

const LayoutPage = () => {
  return <Layout />;
};

const App = (props) => {
  return <AuthPage />;
};

export default LayoutPage;
