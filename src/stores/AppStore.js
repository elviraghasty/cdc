import React from "react";

export const USER_LOGIN = "USER_LOGIN";
export const USER_LOGOUT = "USER_LOGOUT";
export const USER_SIGNUP = "USER_SIGNUP";
export const USER_SIGNUPCONFIRMATION = "USER_SIGNUPCONFIRMATION";
export const USER_FORGOTPASSWORD = "USER_FORGOTPASSWORD";
export const USER_CONFIRMFORGOTPASSWORD = "USER_CONFIRMFORGOTPASSWORD";
export const USER_RESENDCODE = "USER_RESENDCODE";
export const USER_CHANGEPASSWORD = "USER_CHANGEPASSWORD";
export const USER_UPDATE = "USER_UPDATE";
export const SET_ERROR = "SET_ERROR";

export const AppStore = React.createContext();

const initialState = {
  userData: {},
  token: {},
  error: {},
  errorMessage: null,
  isAuthenticated: false,
};

function reducer(state, action) {
  switch (action.type) {
    case USER_LOGIN:
      return {
        ...state,
        isAuthenticated: true,
        userData: action.payload.userData,
        token: action.payload.token,
      };
    case USER_LOGOUT:
    case USER_CHANGEPASSWORD:
      return {
        ...state,
        isAuthenticated: false,
        userData: action.payload.userData,
        token: action.payload.token,
      };
    case USER_SIGNUP:
    case USER_FORGOTPASSWORD:
      return {
        ...state,
        isAuthenticated: false,
        userData: action.payload.userData,
      };
    case USER_SIGNUPCONFIRMATION:
      return {
        ...state,
        isAuthenticated: true,
        token: action.payload.token,
      };
    case USER_CONFIRMFORGOTPASSWORD:
    case USER_RESENDCODE:
      return {
        ...state,
        isAuthenticated: false,
      };
    case USER_UPDATE:
      return {
        ...state,
        userData: action.payload.userData,
      };
    case SET_ERROR:
      return {
        ...state,
        error: action.error,
        errorMessage: action.errorMessage,
      };
    default:
      return state;
  }
}

export function AppStoreProvider(props) {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  const value = { state, dispatch };
  return <AppStore.Provider value={value}>{props.children}</AppStore.Provider>;
}
