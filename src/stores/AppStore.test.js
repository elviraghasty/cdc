import React, { useContext } from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import {
  AppStore,
  AppStoreProvider,
  SET_ERROR,
  USER_LOGIN,
  USER_SIGNUP,
  USER_SIGNUPCONFIRMATION,
  USER_LOGOUT,
  USER_CHANGEPASSWORD,
  USER_FORGOTPASSWORD,
  USER_CONFIRMFORGOTPASSWORD,
  USER_RESENDCODE,
} from "./AppStore";

test("App Store is listed correctly", () => {
  const TestConsumer = () => (
    <AppStore.Consumer>
      {(value) => <div>My Name Is: {value}</div>}
    </AppStore.Consumer>
  );

  render(
    <AppStore.Provider value="C3P0">
      <TestConsumer />
    </AppStore.Provider>
  );

  expect(screen.getByText(/^My Name Is:/)).toHaveTextContent(
    "My Name Is: C3P0"
  );
});

test("AppStoreProvider is listed correctly with SET_ERROR action in reducer", () => {
  const TestPage = () => {
    const { state, dispatch } = useContext(AppStore);

    const dispatchSetError = () => {
      const testError = { errormessage: "Test Error Message" };
      dispatch({
        type: SET_ERROR,
        error: testError,
      });
    };

    return (
      <div>
        <div>{state.error ? state.error.errormessage : "no error message"}</div>
        <button onClick={dispatchSetError}>set error message</button>
      </div>
    );
  };

  render(
    <AppStoreProvider>
      <TestPage />
    </AppStoreProvider>
  );

  expect(screen.queryByText("set error message")).toBeInTheDocument();

  fireEvent.click(screen.getByText(/set error message/));

  expect(screen.queryByText("Test Error Message")).toBeInTheDocument();
});

test("AppStoreProvider is listed correctly with USER_LOGIN action in reducer", () => {
  const testName = "John Due";
  const testIdTOken = "123asd123sad123sad213";
  const TestPage = () => {
    const { state, dispatch } = useContext(AppStore);

    const dispatchLogin = () => {
      const testPayload = {
        userData: {
          name: testName,
        },
        token: {
          idToken: testIdTOken,
        },
      };
      dispatch({
        type: USER_LOGIN,
        payload: testPayload,
      });
    };

    return (
      <div>
        <div>{state.userData ? state.userData.name : "no user data"}</div>
        <div>{state.token ? state.token.idToken : "no token id"}</div>
        <div>
          {state.isAuthenticated
            ? "User Authenticated"
            : "User not Authenticated"}
        </div>
        <button onClick={dispatchLogin}>act Login</button>
      </div>
    );
  };

  render(
    <AppStoreProvider>
      <TestPage />
    </AppStoreProvider>
  );

  expect(screen.queryByText("act Login")).toBeInTheDocument();
  expect(screen.queryByText("User not Authenticated")).toBeInTheDocument();

  fireEvent.click(screen.getByText(/act Login/));

  expect(screen.queryByText("User Authenticated")).toBeInTheDocument();
  expect(screen.queryByText(testIdTOken)).toBeInTheDocument();
  expect(screen.queryByText(testName)).toBeInTheDocument();
});

test("AppStoreProvider is listed correctly with USER_LOGOUT action in reducer", () => {
  const testName = "emptyName";
  const testIdTOken = "emptyToken";
  const TestPage = () => {
    const { state, dispatch } = useContext(AppStore);

    const dispatchLogin = () => {
      const testPayload = {
        userData: {
          name: testName,
        },
        token: {
          idToken: testIdTOken,
        },
      };
      dispatch({
        type: USER_LOGOUT,
        payload: testPayload,
      });
    };

    return (
      <div>
        <div>{state.userData ? state.userData.name : "no user data"}</div>
        <div>{state.token ? state.token.idToken : "no token id"}</div>
        <div>
          {state.isAuthenticated
            ? "User Authenticated"
            : "User not Authenticated"}
        </div>
        <button onClick={dispatchLogin}>act Logout</button>
      </div>
    );
  };

  render(
    <AppStoreProvider>
      <TestPage />
    </AppStoreProvider>
  );

  expect(screen.queryByText("act Logout")).toBeInTheDocument();
  expect(screen.queryByText("User not Authenticated")).toBeInTheDocument();

  fireEvent.click(screen.getByText(/act Logout/));

  expect(screen.queryByText("User not Authenticated")).toBeInTheDocument();
  expect(screen.queryByText(testIdTOken)).toBeInTheDocument();
  expect(screen.queryByText(testName)).toBeInTheDocument();
});

test("AppStoreProvider is listed correctly with USER_CHANGEPASSWORD action in reducer", () => {
  const testName = "emptyName";
  const testIdTOken = "emptyToken";
  const TestPage = () => {
    const { state, dispatch } = useContext(AppStore);

    const dispatchLogin = () => {
      const testPayload = {
        userData: {
          name: testName,
        },
        token: {
          idToken: testIdTOken,
        },
      };
      dispatch({
        type: USER_CHANGEPASSWORD,
        payload: testPayload,
      });
    };

    return (
      <div>
        <div>{state.userData ? state.userData.name : "no user data"}</div>
        <div>{state.token ? state.token.idToken : "no token id"}</div>
        <div>
          {state.isAuthenticated
            ? "User Authenticated"
            : "User not Authenticated"}
        </div>
        <button onClick={dispatchLogin}>act change password</button>
      </div>
    );
  };

  render(
    <AppStoreProvider>
      <TestPage />
    </AppStoreProvider>
  );

  expect(screen.queryByText("act change password")).toBeInTheDocument();
  expect(screen.queryByText("User not Authenticated")).toBeInTheDocument();

  fireEvent.click(screen.getByText(/act change password/));

  expect(screen.queryByText("User not Authenticated")).toBeInTheDocument();
  expect(screen.queryByText(testIdTOken)).toBeInTheDocument();
  expect(screen.queryByText(testName)).toBeInTheDocument();
});

test("AppStoreProvider is listed correctly with USER_SIGNUP action in reducer", () => {
  const testName = "signUpUser";
  const TestPage = () => {
    const { state, dispatch } = useContext(AppStore);

    const dispatchLogin = () => {
      const testPayload = {
        userData: {
          name: testName,
        },
      };
      dispatch({
        type: USER_SIGNUP,
        payload: testPayload,
      });
    };

    return (
      <div>
        <div>{state.userData ? state.userData.name : "no user data"}</div>
        <div>
          {state.isAuthenticated
            ? "User Authenticated"
            : "User not Authenticated"}
        </div>
        <button onClick={dispatchLogin}>act signup</button>
      </div>
    );
  };

  render(
    <AppStoreProvider>
      <TestPage />
    </AppStoreProvider>
  );

  expect(screen.queryByText("act signup")).toBeInTheDocument();
  expect(screen.queryByText("User not Authenticated")).toBeInTheDocument();

  fireEvent.click(screen.getByText(/act signup/));

  expect(screen.queryByText("User not Authenticated")).toBeInTheDocument();
  expect(screen.queryByText(testName)).toBeInTheDocument();
});

test("AppStoreProvider is listed correctly with USER_SIGNUPCONFIRMATION action in reducer", () => {
  const testIdTOken = "123asd123sad123sad213";
  const TestPage = () => {
    const { state, dispatch } = useContext(AppStore);

    const dispatchLogin = () => {
      const testPayload = {
        token: {
          idToken: testIdTOken,
        },
      };
      dispatch({
        type: USER_SIGNUPCONFIRMATION,
        payload: testPayload,
      });
    };

    return (
      <div>
        <div>{state.token ? state.token.idToken : "no token id"}</div>
        <div>
          {state.isAuthenticated
            ? "User Authenticated"
            : "User not Authenticated"}
        </div>
        <button onClick={dispatchLogin}>act USER_SIGNUPCONFIRMATION</button>
      </div>
    );
  };

  render(
    <AppStoreProvider>
      <TestPage />
    </AppStoreProvider>
  );

  expect(screen.queryByText("act USER_SIGNUPCONFIRMATION")).toBeInTheDocument();
  expect(screen.queryByText("User not Authenticated")).toBeInTheDocument();

  fireEvent.click(screen.getByText(/act USER_SIGNUPCONFIRMATION/));

  expect(screen.queryByText("User Authenticated")).toBeInTheDocument();
  expect(screen.queryByText(testIdTOken)).toBeInTheDocument();
});

test("AppStoreProvider is listed correctly with USER_FORGOTPASSWORD action in reducer", () => {
  const testName = "forgotUser";
  const TestPage = () => {
    const { state, dispatch } = useContext(AppStore);

    const dispatchLogin = () => {
      const testPayload = {
        userData: {
          name: testName,
        },
      };
      dispatch({
        type: USER_FORGOTPASSWORD,
        payload: testPayload,
      });
    };

    return (
      <div>
        <div>{state.userData ? state.userData.name : "no user data"}</div>
        <div>
          {state.isAuthenticated
            ? "User Authenticated"
            : "User not Authenticated"}
        </div>
        <button onClick={dispatchLogin}>act USER_FORGOTPASSWORD</button>
      </div>
    );
  };

  render(
    <AppStoreProvider>
      <TestPage />
    </AppStoreProvider>
  );

  expect(screen.queryByText("act USER_FORGOTPASSWORD")).toBeInTheDocument();
  expect(screen.queryByText("User not Authenticated")).toBeInTheDocument();

  fireEvent.click(screen.getByText(/act USER_FORGOTPASSWORD/));

  expect(screen.queryByText("User not Authenticated")).toBeInTheDocument();
  expect(screen.queryByText(testName)).toBeInTheDocument();
});

test("AppStoreProvider is listed correctly with USER_CONFIRMFORGOTPASSWORD action in reducer", () => {
  const TestPage = () => {
    const { state, dispatch } = useContext(AppStore);

    const dispatchLogin = () => {
      dispatch({
        type: USER_CONFIRMFORGOTPASSWORD,
      });
    };

    return (
      <div>
        <div>
          {state.isAuthenticated
            ? "User Authenticated"
            : "User not Authenticated"}
        </div>
        <button onClick={dispatchLogin}>act USER_CONFIRMFORGOTPASSWORD</button>
      </div>
    );
  };

  render(
    <AppStoreProvider>
      <TestPage />
    </AppStoreProvider>
  );

  expect(
    screen.queryByText("act USER_CONFIRMFORGOTPASSWORD")
  ).toBeInTheDocument();
  expect(screen.queryByText("User not Authenticated")).toBeInTheDocument();

  fireEvent.click(screen.getByText(/act USER_CONFIRMFORGOTPASSWORD/));

  expect(screen.queryByText("User not Authenticated")).toBeInTheDocument();
});

test("AppStoreProvider is listed correctly with USER_RESENDCODE action in reducer", () => {
  const TestPage = () => {
    const { state, dispatch } = useContext(AppStore);

    const dispatchLogin = () => {
      dispatch({
        type: USER_RESENDCODE,
      });
    };

    return (
      <div>
        <div>
          {state.isAuthenticated
            ? "User Authenticated"
            : "User not Authenticated"}
        </div>
        <button onClick={dispatchLogin}>act USER_RESENDCODE</button>
      </div>
    );
  };

  render(
    <AppStoreProvider>
      <TestPage />
    </AppStoreProvider>
  );

  expect(screen.queryByText("act USER_RESENDCODE")).toBeInTheDocument();
  expect(screen.queryByText("User not Authenticated")).toBeInTheDocument();

  fireEvent.click(screen.getByText(/act USER_RESENDCODE/));

  expect(screen.queryByText("User not Authenticated")).toBeInTheDocument();
});
