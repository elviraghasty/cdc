import React from "react";
import { Link, Route, Router, Switch } from "react-router-dom";
import { createMemoryHistory } from "history";
import { render, screen, fireEvent } from "@testing-library/react";
import App from "./App";
import SignIn from "./pages/authentication/SignIn";
import HomePage from "./pages/HomePage";

function renderWithRouter(
  ui,
  {
    route = "/",
    history = createMemoryHistory({ initialEntries: [route] }),
  } = {}
) {
  return {
    ...render(<Router history={history}>{ui}</Router>),
    // adding `history` to the returned utilities to allow us
    // to reference it in our tests (just try to avoid using
    // this to test implementation details).
    history,
  };
}

function sum(a, b) {
  return a + b;
}

test("full app rendering/navigating", () => {
  //const { container } = renderWithRouter(<App />);

  expect(sum(1, 2)).toBe(3);
  //expect(container.innerHTML).toMatch(<SignIn />)
});
