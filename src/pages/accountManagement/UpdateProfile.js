import React, { useState, useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { Alert } from "react-bootstrap";

import Config from "./../../config/Config";
import { AppStore, USER_UPDATE, SET_ERROR } from "../../stores/AppStore";
import NeedToLogin from "./../../layout/NeedToLogin";

import "./../../assets/css/stylesheet.css";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";

const axios = require("axios");

function UpdateProfile() {
  const { state, dispatch } = useContext(AppStore);
  const history = useHistory();
  const { handleSubmit, errors, register, setValue } = useForm({
    mode: "all",
  });
  const onButtonClicked = () => {};
  const [showAlert, setAlert] = useState(false);
  const [birthDate, setBirthDate] = useState(new Date());
  const [datee, setDatee] = useState(moment(new Date()).format("DD-MM-yyyy"));

  const [accessToken] = useState(state.token.accessToken);

  const onCancelClicked = () => {
    history.push("/acct/userprofile");
  };

  const onValidateBirthdate = (value) => {
    const dateArr = value.split("-");
    const dateVal = new Date(dateArr[2], dateArr[1] - 1, dateArr[0]);
    const nowDate = new Date();

    return dateVal <= nowDate || "Birthdate is in future date";
  };

  useEffect(() => {
    const setDatePickerValue = (date) => {
      const dateArr = date.split("-");
      const dateVal = new Date(dateArr[2], dateArr[1] - 1, dateArr[0]);

      handleDateChange(dateVal);
    };

    const getUserProfile = async () => {
      //call API
      var urlLink =
        Config.apiBasePath + Config.authPath + Config.authPathGetUserByToken;
      const requestData = { accessToken: accessToken };
      const result = await axios.post(urlLink, requestData);

      setValue("name", result.data.data.name);
      setValue("middleName", result.data.data.middleName);
      setValue("address", result.data.data.address);
      setValue("email", result.data.data.email);
      setValue("gender", capitalize(result.data.data.gender));

      setDatePickerValue(result.data.data.birthDate);
      //console.log(eventList);
    };

    getUserProfile();
  }, [accessToken, setValue]);

  const handleDateChange = (date) => {
    setDatee(moment(date).format("DD-MM-yyyy"));
    setBirthDate(date);
  };

  const capitalize = (s) => {
    if (typeof s !== "string") return "";
    return s.charAt(0).toUpperCase() + s.slice(1);
  };

  const onSubmit = (data) => {
    //call API
    var urlLink =
      Config.apiBasePath + Config.authPath + Config.authPathUpdateUser;

    const requestData = {
      username: data.email,
      address: data.address,
      birthdate: datee,
      gender: data.gender,
      middleName: data.middleName,
      name: data.name,
      accessToken: accessToken,
    };

    axios
      .post(urlLink, requestData)
      .then((response) => {
        //console.log(response);

        const payload = {
          userData: {
            username: data.email,
            email: data.email,
            name: data.name,
            address: data.address,
            birthDate: requestData.birthdate,
            gender: data.gender,
            middleName: data.middleName,
          },
        };

        dispatch({
          type: USER_UPDATE,
          payload: payload,
        });

        alert("Update User success");

        history.push("/acct/userprofile");
      })
      .catch((err) => {
        if (err.response) {
          // client received an error response (5xx, 4xx)
          dispatch({
            type: SET_ERROR,
            error: err.response,
            errorMessage:
              err.response.status === 400
                ? "Bad Request!"
                : err.response.data.errors[0].message,
          });
          setAlert(true);
        } else if (err.request) {
          // client never received a response, or request never left
          console.log(err.request);
          alert("Error Reqeust API, please see log console for more details.");
        } else {
          // anything else
        }
      });
  };

  return (
    <main>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="auth-wrapper">
          <div className="auth-content">
            <div className="card">
              <div className="card-body text-center">
                <h3 className="mb-4">Update Profile</h3>
                {!state.isAuthenticated ? (
                  <NeedToLogin />
                ) : (
                  <div>
                    <div className="mb-2">
                      {state.error ? (
                        <Alert
                          show={showAlert}
                          onClose={() => setAlert(false)}
                          dismissible
                          variant="danger"
                        >
                          {state.errorMessage}
                        </Alert>
                      ) : null}
                      <label className="input-group labelForm" name="lblName">
                        Name
                      </label>
                      <div className="input-group">
                        <input
                          name="name"
                          type="text"
                          className="form-control"
                          placeholder="Name"
                          ref={register({
                            required: "Name is required",
                            validate: (value) => {
                              return !!value.trim() || "Name is required";
                            },
                            maxLength: {
                              value: 50,
                              message: "Name should not exceed 50 characters",
                            },
                          })}
                        />
                      </div>
                      <span className="error">
                        {errors.name && errors.name.message}
                      </span>
                    </div>

                    <div className="mb-2">
                      <label
                        className="input-group labelForm"
                        name="lblMiddleName"
                      >
                        Middle Name
                      </label>
                      <div className="input-group">
                        <input
                          name="middleName"
                          type="text"
                          className="form-control"
                          placeholder="Middle Name"
                          ref={register({
                            required: "Middle Name is required",
                            validate: (value) => {
                              return (
                                !!value.trim() || "Middle Name is required"
                              );
                            },
                            maxLength: {
                              value: 50,
                              message:
                                "Middle Name should not exceed 50 characters",
                            },
                          })}
                        />
                      </div>
                      <span className="error">
                        {errors.middleName && errors.middleName.message}
                      </span>
                    </div>

                    <div className="mb-2">
                      <label className="input-group labelForm" name="lblEmail">
                        E-mail
                      </label>
                      <div className="input-group">
                        <input
                          disabled
                          name="email"
                          type="email"
                          className="form-control"
                          ref={register({
                            required: "Email is required",
                            pattern: {
                              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                              message: "Invalid email address",
                            },
                          })}
                        />
                      </div>
                      <span className="error">
                        {errors.email && errors.email.message}
                      </span>
                    </div>

                    <div className="mb-2">
                      <label
                        className="input-group labelForm"
                        name="lblAddress"
                      >
                        Address
                      </label>
                      <div className="input-group">
                        <input
                          name="address"
                          type="text"
                          className="form-control"
                          placeholder="Address"
                          ref={register({
                            required: "Address is required",
                            validate: (value) => {
                              return !!value.trim() || "Address is required";
                            },
                            maxLength: {
                              value: 200,
                              message:
                                "Address should not exceed 200 characters",
                            },
                          })}
                        />
                      </div>
                      <span className="error">
                        {errors.address && errors.address.message}
                      </span>
                    </div>

                    <div className="mb-2">
                      <label
                        className="input-group labelForm"
                        name="lblBirthdate"
                      >
                        Birthdate
                      </label>
                      <div className="input-group">
                        <DatePicker
                          name="birthdatePicker"
                          selected={birthDate}
                          onChange={handleDateChange}
                          placeholderText="Birthdate"
                          className="form-control"
                        />
                        <input
                          name="birthdate"
                          type="hidden"
                          className="form-control"
                          value={datee}
                          ref={register({
                            required: "Birthdate is required",
                            validate: (value) => onValidateBirthdate(value),
                            maxLength: {
                              value: 10,
                              message: "Birthdate is invalid",
                            },
                          })}
                          onChange={handleDateChange}
                        />
                      </div>
                      <span className="error">
                        {errors.birthdate && errors.birthdate.message}
                      </span>
                    </div>

                    <div className="mb-2">
                      <label className="input-group labelForm" name="lblGender">
                        Gender
                      </label>
                      <div className="input-group">
                        <label className="mr-5">
                          <input
                            name="gender"
                            type="radio"
                            value="Male"
                            ref={register({ required: true })}
                          />{" "}
                          Male
                        </label>
                        <label>
                          <input
                            name="gender"
                            type="radio"
                            value="Female"
                            ref={register({ required: true })}
                          />{" "}
                          Female
                        </label>
                      </div>
                      <span className="error">
                        {errors.gender?.type === "required" &&
                          "Gender is required"}
                      </span>
                    </div>

                    <button
                      className="btn btn-primary shadow-2 mb-4"
                      onClick={onButtonClicked}
                    >
                      Update
                    </button>
                    {"  "}
                    <button
                      className="btn btn-secondary shadow-2 mb-4"
                      onClick={onCancelClicked}
                    >
                      Cancel
                    </button>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </form>
    </main>
  );
}

export default UpdateProfile;
