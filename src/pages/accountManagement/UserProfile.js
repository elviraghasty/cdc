import React, { useContext } from "react";
import { Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { AppStore, USER_LOGOUT, SET_ERROR } from "./../../stores/AppStore";
import Config from "./../../config/Config";

const axios = require("axios");

function UserProfile() {
  const history = useHistory();
  const { state, dispatch } = useContext(AppStore);

  const onUpdateProfileClick = () => {
    history.push("updateprofile");
  };

  const onLogOutClick = () => {
    const userDataFromContext = state.userData;
    const requestData = {
      username: userDataFromContext.username,
      accessToken: state.token.accessToken,
    };

    //call API
    var urlLink = Config.apiBasePath + Config.authPath + Config.authPathSignOut;

    axios
      .post(urlLink, requestData)
      .then((response) => {
        //console.log(response);

        const payload = {
          userData: {
            username: "",
            email: "",
            name: "",
            address: "",
            birthDate: "",
            gender: "",
            middleName: "",
          },
          token: { idToken: "", accessToken: "" },
        };

        dispatch({
          type: USER_LOGOUT,
          payload: payload,
        });

        history.push("/");
      })
      .catch((err) => {
        if (err.response) {
          // client received an error response (5xx, 4xx)
          dispatch({
            type: SET_ERROR,
            error: err.response,
          });

          //console.log(err.response);
          var errorStatus = err.response.status;
          var errorMessage = err.response.data.errors[0].message;
          alert("(" + errorStatus + ") " + errorMessage);
        } else if (err.request) {
          // client never received a response, or request never left
          console.log(err.request);
          alert("Error Reqeust API, please see log console for more details.");
        } else {
          // anything else
        }
      });
  };

  return (
    <main>
      <div className="card">
        <div className="card-body text-center">
          <h3 className="mb-4">User Profile</h3>

          <div>
            <div>
              <label className="input-group labelForm">
                Username: {state.userData.username}
              </label>
            </div>
            <div>
              <label className="input-group labelForm">
                Name: {state.userData.name} {state.userData.middleName}
              </label>
            </div>
            <div>
              <label className="input-group labelForm">
                Gender: {state.userData.gender}
              </label>
            </div>
            <Button
              variant="secondary"
              onClick={onUpdateProfileClick}
              className="mb-2"
            >
              Update Profile
            </Button>
            <br></br>
            <button onClick={onLogOutClick} className="btn btn-primary">
              Log Out
            </button>
          </div>
        </div>
      </div>
    </main>
  );
}
export default UserProfile;
