import React from "react";
import { render } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";

import Dashboard from "./Dashboard";
import { AppStore, AppStoreProvider } from "./../../stores/AppStore";

test("Dashboard rendering correctly", () => {
  const { getByTestId } = render(
    <AppStoreProvider>
      <Router>
        <Dashboard />
      </Router>
    </AppStoreProvider>
  );

  const elmContainer = getByTestId("cont-dashboard");
  expect(elmContainer).toBeInTheDocument();
});
/*
test("Dashboard rendering correctly with store", () => {
  const state = {
    userData: {},
    token: { idToken: "testToken" },
    error: {},
    isAuthenticated: false,
  };

  const { debug, getByTestId } = render(
    <AppStore.Provider value={state}>
      <Router>
        <Dashboard />
      </Router>
    </AppStore.Provider>
  );

  debug();

  const elmContainer = getByTestId("cont-dashboard");
  expect(elmContainer).toBeInTheDocument();
});
*/
