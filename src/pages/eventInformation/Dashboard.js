import React, { useState, useContext, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";

import { AppStore } from "./../../stores/AppStore";
import Config from "./../../config/Config";
import CardEventList from "./../../layout/CardEventList";

const axios = require("axios");

function Dashboard() {
  const { state } = useContext(AppStore);

  const [idToken] = useState(state.token.idToken);
  const [dispEventList, setDispEventList] = useState([]);

  const chunkSize = 4;

  function chunkArray(myArray, chunk_size) {
    var results = [];

    while (myArray.length) {
      results.push(myArray.splice(0, chunk_size));
    }

    return results;
  }

  useEffect(() => {
    const retrieveEvents = async () => {
      //call API
      var urlLink =
        Config.apiBasePath + Config.eventPath + Config.eventPathGetAllEvents;
      const result = await axios.get(urlLink);

      setDispEventList(chunkArray(result.data.data, chunkSize));
      //console.log(eventList);
    };

    retrieveEvents();
  }, [idToken]);

  return (
    <main>
      <Container fluid data-testid="cont-dashboard">
        {dispEventList.map((eventRow) => (
          <Row key={eventRow.id} md={chunkSize}>
            {eventRow.map((eventItem) => (
              <Col>
                <CardEventList>{eventItem}</CardEventList>
              </Col>
            ))}
          </Row>
        ))}
      </Container>
    </main>
  );
}

export default Dashboard;
