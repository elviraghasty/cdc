import React, { useState, useContext, useEffect } from "react";
import { useHistory, Link } from "react-router-dom";
import { Alert } from "react-bootstrap";

import Config from "./../../config/Config";
import {
  AppStore,
  USER_SIGNUPCONFIRMATION,
  USER_RESENDCODE,
  SET_ERROR,
} from "../../stores/AppStore";

import "./../../assets/scss/style.scss";
import "./../../assets/css/stylesheet.css";
import ReactCodeInput from "react-verification-code-input";

const axios = require("axios");

function SignUpConfirmation() {
  const history = useHistory();
  const [showAlert, setAlert] = useState(false);
  const { state, dispatch } = useContext(AppStore);

  const onComplete = (data) => {
    const userDataFromContext = state.userData;

    const requestData = {
      confirmationCode: data,
      email: userDataFromContext.email,
      password: userDataFromContext.password,
    };

    //call API
    var urlLink =
      Config.apiBasePath + Config.authPath + Config.authPathSignUpConfirmation;

    axios
      .post(urlLink, requestData)
      .then((response) => {
        //console.log(response);

        const payload = {
          token: {
            idToken: response.data.data.idToken,
            accessToken: response.data.data.accessToken,
          },
        };

        dispatch({
          type: USER_SIGNUPCONFIRMATION,
          payload: payload,
        });

        alert("Verification success");

        history.push("/");
      })
      .catch((err) => {
        if (err.response) {
          // client received an error response (5xx, 4xx)
          dispatch({
            type: SET_ERROR,
            error: err.response,
            errorMessage: err.response.status === 400 ? "Bad Request!" : err.response.data.errors[0].message,
          });
          setAlert(true);
        } else if (err.request) {
          // client never received a response, or request never left
          console.log(err.request);
          alert("Error Reqeust API, please see log console for more details.");
        } else {
          // anything else
        }
      });
  };

  const onClickResendCode = () => {
    const userDataFromContext = state.userData;

    //call API
    var urlLink =
      Config.apiBasePath +
      Config.authPath +
      Config.authPathResendConfirmationCode;

    const requestData = {
      username: userDataFromContext.username,
    };

    axios
      .post(urlLink, requestData)
      .then((response) => {
        //console.log(response);

        dispatch({
          type: USER_RESENDCODE,
        });

        alert("resend code is success!");
      })
      .catch((err) => {
        if (err.response) {
          // client received an error response (5xx, 4xx)
          dispatch({
            type: SET_ERROR,
            error: err.response,
            errorMessage: err.response.status === 400 ? "Bad Request!" : err.response.data.errors[0].message,
          });
          setAlert(true);
        } else if (err.request) {
          // client never received a response, or request never left
          console.log(err.request);
          alert("Error Reqeust API, please see log console for more details.");
        } else {
          // anything else
        }
      });
  };

  useEffect(() => {
    console.log(state);
  }, [state]);

  return (
    <main>
      <form>
        <div className="auth-wrapper">
          <div className="auth-content">
            <div className="card">
              <div className="card-body text-center">
                <img
                  src={require("./../../pictures/greencheck.png")}
                  width="50px"
                  height="50px"
                  alt="green check"
                />
                <h3 className="mb-4">You have sign up successfully</h3>
                <div className="mb-2">
                  <p>
                    Thank you for choosing E-Ems, please check your email for
                    the verification code.
                  </p>
                  {state.error ? ( 
                      <Alert show={showAlert} onClose={() => setAlert(false)} dismissible variant="danger">
                        {state.errorMessage}
                      </Alert>
                  ) : (null)}
                  <div className="input-group">
                    <ReactCodeInput
                      name="confirmationCode"
                      fieldWidth={50}
                      type="number"
                      onComplete={onComplete}
                    />
                  </div>
                  <p className="mb-3 text-muted">
                    Didn't receive a code?
                    <Link onClick={onClickResendCode}> Resend code</Link>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </main>
  );
}

export default SignUpConfirmation;
