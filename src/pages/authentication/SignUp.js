import React, { useState, useContext, useEffect } from "react";
import { NavLink, useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { Alert } from "react-bootstrap";

import Config from "./../../config/Config";
import { AppStore, USER_SIGNUP, SET_ERROR } from "../../stores/AppStore";

//import "./../../assets/scss/style.scss";
import "./../../assets/css/stylesheet.css";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";

const axios = require("axios");

function SignUp() {
  const { state, dispatch } = useContext(AppStore);
  const history = useHistory();
  const { handleSubmit, errors, register } = useForm({
    mode: "all",
  });
  const onButtonClicked = () => {};
  const [showAlert, setAlert] = useState(false);
  const [birthDate, setBirthDate] = useState(new Date());
  const [datee, setDatee] = useState(moment(new Date()).format("DD-MM-yyyy"));

  useEffect(() => {
    console.log(state);
  }, [state]);

  const handleChange = (date) => {
    setDatee(moment(date).format("DD-MM-yyyy"));
    setBirthDate(date);
  };

  const onSubmit = (data) => {
    //call API
    var urlLink = Config.apiBasePath + Config.authPath + Config.authPathSignUp;

    axios
      .post(urlLink, data)
      .then((response) => {
        //console.log(response);

        const payload = {
          userData: {
            username: response.data.data.email,
            email: response.data.data.email,
            name: data.name,
            password: data.password,
            address: data.address,
            birthDate: data.birthdate,
            gender: data.gender,
            middleName: data.middleName,
            userRole: response.data.data.userRole,
          },
        };

        dispatch({
          type: USER_SIGNUP,
          payload: payload,
        });

        history.push("/auth/signupconfirmation");
      })
      .catch((err) => {
        if (err.response) {
          // client received an error response (5xx, 4xx)
          dispatch({
            type: SET_ERROR,
            error: err.response,
            errorMessage: err.response.data.errors[0].message,
          });
          setAlert(true);
        } else if (err.request) {
          // client never received a response, or request never left
          console.log(err.request);
          alert("Error Reqeust API, please see log console for more details.");
        } else {
          // anything else
        }
      });
  };

  return (
    <main>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="auth-wrapper">
          <div className="auth-content">
            <div className="card">
              <div className="card-body text-center">
                <h3 className="welcomeFont">Welcome to E-ems</h3>
                <h3 className="mb-4">Sign up</h3>
                <div className="mb-2">
                  {state.error ? ( 
                      <Alert show={showAlert} onClose={() => setAlert(false)} dismissible variant="danger">
                        {state.errorMessage}
                      </Alert>
                  ) : (null)}
                  <label className="input-group labelForm" name="lblName">
                    Name
                  </label>
                  <div className="input-group">
                    <input
                      name="name"
                      type="text"
                      className="form-control"
                      placeholder="Name"
                      ref={register({
                        required: "Name is required",
                        maxLength: {
                          value: 50,
                          message: "Name should not exceed 50 characters",
                        },
                      })}
                    />
                  </div>
                  <span className="error">
                    {errors.name && errors.name.message}
                  </span>
                </div>

                <div className="mb-2">
                  <label className="input-group labelForm" name="lblMiddleName">
                    Middle Name
                  </label>
                  <div className="input-group">
                    <input
                      name="middleName"
                      type="text"
                      className="form-control"
                      placeholder="Middle Name"
                      ref={register({
                        required: "Middle Name is required",
                        maxLength: {
                          value: 50,
                          message:
                            "Middle Name should not exceed 50 characters",
                        },
                      })}
                    />
                  </div>
                  <span className="error">
                    {errors.middleName && errors.middleName.message}
                  </span>
                </div>

                <div className="mb-2">
                  <label className="input-group labelForm" name="lblEmail">
                    E-mail
                  </label>
                  <div className="input-group">
                    <input
                      name="email"
                      type="email"
                      className="form-control"
                      placeholder="i.e. : user@mail.com"
                      ref={register({
                        required: "Email is required",
                        pattern: {
                          value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                          message: "Invalid email address",
                        },
                      })}
                    />
                  </div>
                  <span className="error">
                    {errors.email && errors.email.message}
                  </span>
                </div>

                <div className="mb-2">
                  <label className="input-group labelForm" name="lblAddress">
                    Address
                  </label>
                  <div className="input-group">
                    <input
                      name="address"
                      type="text"
                      className="form-control"
                      placeholder="Address"
                      ref={register({ required: true })}
                    />
                  </div>
                  <span className="error">
                    {errors.address?.type === "required" &&
                      "Address is required"}
                  </span>
                </div>

                <div className="mb-2">
                  <label className="input-group labelForm" name="lblBirthdate">
                    Birthdate
                  </label>
                  <div className="input-group">
                    <DatePicker
                      name="birthdatePicker"
                      selected={birthDate}
                      onChange={handleChange}
                      placeholderText="Birthdate"
                      className="form-control"
                    />
                    <input
                      name="birthdate"
                      type="hidden"
                      className="form-control"
                      value={datee}
                      ref={register({ required: true })}
                      onChange={handleChange}
                    />
                  </div>
                  <span className="error">
                    {errors.birthdate?.type === "required" &&
                      "Birthdate is required"}
                  </span>
                </div>

                <div className="mb-2">
                  <label className="input-group labelForm" name="lblGender">
                    Gender
                  </label>
                  <div className="input-group">
                    <label className="mr-5">
                      <input
                        name="gender"
                        type="radio"
                        value="Male"
                        ref={register({ required: true })}
                      />{" "}
                      Male
                    </label>
                    <label>
                      <input
                        name="gender"
                        type="radio"
                        value="Female"
                        ref={register({ required: true })}
                      />{" "}
                      Female
                    </label>
                  </div>
                  <span className="error">
                    {errors.gender?.type === "required" && "Gender is required"}
                  </span>
                </div>

                <div className="mb-2">
                  <label className="input-group labelForm" name="lblPassword">
                    Password
                  </label>
                  <div className="input-group">
                    <input
                      name="password"
                      type="password"
                      className="form-control"
                      placeholder="Password"
                      ref={register({
                        required: "Password is required",
                        minLength: {
                          value: 6,
                          message: "Password must have at least 6 characters",
                        },
                        maxLength: {
                          value: 20,
                          message: "Password should not exceed 20 characters",
                        },
                      })}
                    />
                  </div>
                  <span className="error">
                    {errors.password && errors.password.message}
                  </span>
                </div>

                <div className="mb-2">
                  <div className="input-group">
                    <label>
                      <input
                        name="organizer"
                        type="checkbox"
                        className="mr-2"
                        ref={register()}
                      />
                      Set As Organizer
                    </label>
                  </div>
                </div>

                <button
                  className="btn btn-primary shadow-2 mb-4"
                  onClick={onButtonClicked}
                >
                  Sign up
                </button>
                <p className="mb-0 text-muted">
                  Already have an account?{" "}
                  <NavLink to="/auth/signin">Login</NavLink>
                </p>
              </div>
            </div>
          </div>
        </div>
      </form>
    </main>
  );
}

export default SignUp;
