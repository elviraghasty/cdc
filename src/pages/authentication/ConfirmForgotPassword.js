import React, { useState, useContext, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useHistory, Link } from "react-router-dom";
import { Alert } from "react-bootstrap";

import Config from "./../../config/Config";
import {
  AppStore,
  USER_CONFIRMFORGOTPASSWORD,
  USER_RESENDCODE,
  SET_ERROR,
} from "./../../stores/AppStore";

const axios = require("axios");

function ConfirmForgotPassword() {
  const [showAlert, setAlert] = useState(false);
  const { state, dispatch } = useContext(AppStore);
  const history = useHistory();

  const { handleSubmit, register, errors } = useForm();

  //console.log(state);

  const onSubmit = (data) => {
    const userDataFromContext = state.userData;

    //call API
    var urlLink =
      Config.apiBasePath +
      Config.authPath +
      Config.authPathConfirmForgotPassword;

    const requestData = {
      username: userDataFromContext.username,
      newPassword: data.newPassword,
      confirmationCode: data.confirmationCode,
    };

    axios
      .post(urlLink, requestData)
      .then((response) => {
        //console.log(response);

        dispatch({
          type: USER_CONFIRMFORGOTPASSWORD,
        });

        history.push("/");
      })
      .catch((err) => {
        if (err.response) {
          // client received an error response (5xx, 4xx)
          dispatch({
            type: SET_ERROR,
            error: err.response,
            errorMessage: err.response.status === 400 ? "Bad Request!" : err.response.data.errors[0].message,
          });
          setAlert(true);
        } else if (err.request) {
          // client never received a response, or request never left
          console.log(err.request);
          alert("Error Reqeust API, please see log console for more details.");
        } else {
          // anything else
        }
      });
  };

  const onClickResendCode = () => {
    const userDataFromContext = state.userData;

    //call API
    var urlLink =
      Config.apiBasePath +
      Config.authPath +
      Config.authPathResendConfirmationCode;

    const requestData = {
      username: userDataFromContext.username,
    };

    axios
      .post(urlLink, requestData)
      .then((response) => {
        //console.log(response);

        dispatch({
          type: USER_RESENDCODE,
        });

        alert("resend code is success!");
      })
      .catch((err) => {
        if (err.response) {
          // client received an error response (5xx, 4xx)
          dispatch({
            type: SET_ERROR,
            error: err.response,
            errorMessage: err.response.status === 400 ? "Bad Request!" : err.response.data.errors[0].message,
          });
          setAlert(true);
        } else if (err.request) {
          // client never received a response, or request never left
          console.log(err.request);
          alert("Error Reqeust API, please see log console for more details.");
        } else {
          // anything else
        }
      });
  };

  useEffect(() => {
    console.log(state);
  }, [state]);

  const onClicksubmit = () => {};

  return (
    <main>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="auth-wrapper">
          <div className="auth-content">
            <div className="card">
              <div className="card-body text-center">
                <h3 className="mb-4">Confirm Forgot Password?</h3>
                  {state.error ? ( 
                      <Alert show={showAlert} onClose={() => setAlert(false)} dismissible variant="danger">
                        {state.errorMessage}
                      </Alert>
                  ) : (null)}

                <div className="mb-2">
                  <label
                    className="input-group labelForm"
                    name="lblNewPassword"
                  >
                    New Password
                  </label>
                  <div className="input-group">
                    <input
                      name="newPassword"
                      type="Password"
                      className="form-control"
                      placeholder="New Password"
                      ref={register({ required: true })}
                    />
                  </div>
                  <span className="error">
                    {errors.newPassword?.type === "required" &&
                      "New Password is required"}
                  </span>
                </div>
                <div className="mb-2">
                  <label className="input-group labelForm" name="lblConfCode">
                    Confirmation Code
                  </label>
                  <div className="input-group">
                    <input
                      name="confirmationCode"
                      type="text"
                      className="form-control"
                      placeholder="Confirmation Code"
                      ref={register({ required: true })}
                    />
                  </div>
                  <span className="error">
                    {errors.confirmationCode?.type === "required" &&
                      "Confirmation Code is required"}
                  </span>
                </div>
                <p className="mb-3 text-muted">
                  Didn't receive a code?
                  <Link onClick={onClickResendCode}> Resend code</Link>
                </p>
                <button onClick={onClicksubmit} className="btn btn-primary">
                  Reset Password
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </main>
  );
}

export default ConfirmForgotPassword;
