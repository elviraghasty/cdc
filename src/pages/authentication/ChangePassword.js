import React, { useState, useContext, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { Alert } from "react-bootstrap";
import "./../../assets/scss/style.scss";
import "./../../assets/css/stylesheet.css";

import Config from "./../../config/Config";
import {
  AppStore,
  USER_CHANGEPASSWORD,
  SET_ERROR,
} from "../../stores/AppStore";
import NeedToLogin from "./../../layout/NeedToLogin";

const axios = require("axios");

function ChangePassword() {
  const [showAlert, setAlert] = useState(false);
  const { handleSubmit, register, errors, watch } = useForm();
  const newPassword = useRef({});
  newPassword.current = watch("newPassword", "");

  const onButtonClicked = () => {};

  const { state, dispatch } = useContext(AppStore);
  const history = useHistory();

  const onSubmit = (data) => {
    const userDataFromContext = state.userData;
    const tokenFromContext = state.token;

    //call API
    var urlLink =
      Config.apiBasePath + Config.authPath + Config.authPathChangePassword;

    const requestData = {
      username: userDataFromContext.username,

      newPassword: data.newPassword,
      oldPassword: data.oldPassword,
      accessToken: tokenFromContext.accessToken,
    };

    //call service
    axios
      .post(urlLink, requestData)
      .then((response) => {
        //console.log(response);

        const payload = {
          userData: {
            username: "",
            email: "",
            name: "",
            address: "",
            birthDate: "",
            gender: "",
            middleName: "",
          },
          token: { idToken: "", accessToken: "" },
        };

        dispatch({
          type: USER_CHANGEPASSWORD,
          payload: payload,
        });

        alert("change password is success!");
        history.push("/");
      })
      .catch((err) => {
        if (err.response) {
          // client received an error response (5xx, 4xx)
          dispatch({
            type: SET_ERROR,
            error: err.response,
            errorMessage:
              err.response.data.errors[0].message ===
              "Incorrect username or password."
                ? "Please input the correct old password"
                : "(" +
                  err.response.status +
                  ") " +
                  err.response.data.errors[0].message,
          });

          setAlert(true);
        } else if (err.request) {
          // client never received a response, or request never left
          console.log(err.request);
          alert("Error Reqeust API, please see log console for more details.");
        } else {
          // anything else
        }
      });
  };

  useEffect(() => {
    console.log(state);
  }, [state]);

  return (
    <main>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="auth-wrapper">
          <div className="auth-content">
            <div className="card">
              <div className="card-body text-center">
                <h3 className="mb-4">Change Password</h3>
                {!state.isAuthenticated ? (
                  <NeedToLogin />
                ) : (
                  <>
                    <div className="mb-2">
                      {state.error ? (
                        <Alert
                          show={showAlert}
                          onClose={() => setAlert(false)}
                          dismissible
                          variant="danger"
                        >
                          {state.errorMessage}
                        </Alert>
                      ) : null}
                      <p className="mb-2">Username : {state.userData.email}</p>
                      <label
                        className="input-group labelForm"
                        name="lblOldPassword"
                      >
                        Old Password
                      </label>
                      <div className="input-group">
                        <input
                          name="oldPassword"
                          type="password"
                          className="form-control"
                          placeholder="Old Password"
                          ref={register({
                            required: "Old Password is required",
                            minLength: {
                              value: 6,
                              message:
                                "Old Password must have at least 6 characters",
                            },
                            maxLength: {
                              value: 20,
                              message:
                                "Old Password should not exceed 20 characters",
                            },
                          })}
                        />
                      </div>
                      <span className="error">
                        {errors.oldPassword && errors.oldPassword.message}
                      </span>
                    </div>

                    <div className="mb-2">
                      <label
                        className="input-group labelForm"
                        name="lblNewPassword"
                      >
                        New Password
                      </label>
                      <div className="input-group">
                        <input
                          name="newPassword"
                          type="password"
                          className="form-control"
                          placeholder="New Password"
                          ref={register({
                            required: "New Password is required",
                            minLength: {
                              value: 6,
                              message:
                                "New Password must have at least 6 characters",
                            },
                            maxLength: {
                              value: 20,
                              message:
                                "New Password should not exceed 20 characters",
                            },
                          })}
                        />
                      </div>
                      <span className="error">
                        {errors.newPassword && errors.newPassword.message}
                      </span>
                    </div>

                    <div className="mb-4">
                      <label
                        className="input-group labelForm"
                        name="lblConfirmPassword"
                      >
                        Confirm Password
                      </label>
                      <div className="input-group">
                        <input
                          name="confirmPassword"
                          type="password"
                          className="form-control"
                          placeholder="Confirm Password"
                          ref={register({
                            validate: (value) =>
                              value === newPassword.current ||
                              "The confirm password do not match with new password",
                          })}
                        />
                      </div>
                      <span className="error">
                        {errors.confirmPassword &&
                          errors.confirmPassword.message}
                      </span>
                    </div>

                    <button
                      className="btn btn-primary shadow-2 mb-2"
                      onClick={onButtonClicked}
                    >
                      Change Password
                    </button>
                  </>
                )}{" "}
              </div>
            </div>
          </div>
        </div>
      </form>
    </main>
  );
}

export default ChangePassword;
