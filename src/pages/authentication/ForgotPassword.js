import React, { useState, useContext, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { Alert } from "react-bootstrap";

import Config from "./../../config/Config";
import {
  AppStore,
  USER_FORGOTPASSWORD,
  SET_ERROR,
} from "./../../stores/AppStore";

const axios = require("axios");

function ForgotPassword() {
  const [showAlert, setAlert] = useState(false);
  const { state, dispatch } = useContext(AppStore);
  const history = useHistory();
  const { handleSubmit, register, errors } = useForm();

  //console.log(state);

  const onSubmit = (data) => {
    //call API
    var urlLink =
      Config.apiBasePath + Config.authPath + Config.authPathForgotPassword;

    axios
      .post(urlLink, data)
      .then((response) => {
        //console.log(response);

        const payload = {
          userData: {
            username: response.data.data.username,
          },
        };

        dispatch({
          type: USER_FORGOTPASSWORD,
          payload: payload,
        });

        history.push("/auth/confirmforgotpassword");
      })
      .catch((err) => {
        if (err.response) {
          // client received an error response (5xx, 4xx)
          dispatch({
            type: SET_ERROR,
            error: err.response,
            errorMessage: err.response.data.errors[0].message
          });
          setAlert(true);
        } else if (err.request) {
          // client never received a response, or request never left
          console.log(err.request);
          alert("Error Reqeust API, please see log console for more details.");
        } else {
          // anything else
        }
      });
  };

  useEffect(() => {
    console.log(state);
  }, [state]);

  const onClick = () => {};

  return (
    <main>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="auth-wrapper">
          <div className="auth-content">
            <div className="card">
              <div className="card-body text-center">
                <h3 className="mb-4">Forgot Password?</h3>
                <h5 className="mb-0">Enter your email address</h5>
                <p className="mb-1 text-muted">
                  We will send confirmation code to reset password
                </p>
                  {state.error ? ( 
                      <Alert show={showAlert} onClose={() => setAlert(false)} dismissible variant="danger">
                        {state.errorMessage}
                      </Alert>
                  ) : (null)}
                <div className="mb-2">
                  <div className="input-group">
                    <input
                      name="username"
                      type="email"
                      className="form-control"
                      placeholder="email"
                      ref={register({ required: true })}
                    />
                  </div>
                  <span className="error">
                    {errors.username?.type === "required" &&
                      "Email is required"}
                  </span>
                </div>
                <button onClick={onClick} className="btn btn-primary">
                  Submit
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </main>
  );
}

export default ForgotPassword;
