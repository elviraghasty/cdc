import React, { useState, useContext, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useHistory, NavLink, Link } from "react-router-dom";
import { Alert } from "react-bootstrap";

import Config from "./../../config/Config";
import {
  AppStore,
  USER_LOGIN,
  USER_LOGOUT,
  SET_ERROR,
} from "./../../stores/AppStore";

import "./../../assets/scss/style.scss";
const axios = require("axios");

function SignIn() {
  const [showAlert, setAlert] = useState(false);

  const { state, dispatch } = useContext(AppStore);

  const history = useHistory();

  const { handleSubmit, register, errors } = useForm();

  const onLoginClicked = (data) => {
    //call API
    var urlLink = Config.apiBasePath + Config.authPath + Config.authPathSignIn;

    axios
      .post(urlLink, data)
      .then((response) => {
        //console.log(response);

        const payload = {
          userData: response.data.data.data.userData,
          token: {
            accessToken: response.data.data.data.accessToken,
            idToken: response.data.data.data.idToken,
          },
        };

        dispatch({
          type: USER_LOGIN,
          payload: payload,
        });

        history.push("/");
      })
      .catch((err) => {
        if (err.response) {
          // client received an error response (5xx, 4xx)
          dispatch({
            type: SET_ERROR,
            error: err.response,
            errorMessage: err.response.data.errors[0].message
          });
          setAlert(true);
        } else if (err.request) {
          // client never received a response, or request never left
          console.log(err.request);
          alert("Error Reqeust API, please see log console for more details.");
        } else {
          // anything else
        }
      });
  };

  const onClickLogOut = () => {
    const userDataFromContext = state.userData;
    const requestData = {
      username: userDataFromContext.username,
      accessToken: state.token.accessToken,
    };

    //call API
    var urlLink = Config.apiBasePath + Config.authPath + Config.authPathSignOut;

    axios
      .post(urlLink, requestData)
      .then((response) => {
        //console.log(response);

        const payload = {
          userData: {
            username: "",
            email: "",
            name: "",
            address: "",
            birthDate: "",
            gender: "",
            middleName: "",
          },
          token: { idToken: "", accessToken: "" },
        };

        dispatch({
          type: USER_LOGOUT,
          payload: payload,
        });
      })
      .catch((err) => {
        if (err.response) {
          // client received an error response (5xx, 4xx)
          dispatch({
            type: SET_ERROR,
            error: err.response,
          });

          //console.log(err.response);
          var errorStatus = err.response.status;
          var errorMessage = err.response.data.errors[0].message;
          alert("(" + errorStatus + ") " + errorMessage);
        } else if (err.request) {
          // client never received a response, or request never left
          console.log(err.request);
          alert("Error Reqeust API, please see log console for more details.");
        } else {
          // anything else
        }
      });
  };

  const onButtonClicked = () => {};

  useEffect(() => {
    console.log(state);
  }, [state]);

  return (
    <main>
      <form onSubmit={handleSubmit(onLoginClicked)}>
        <div className="auth-wrapper">
          <div className="auth-content">
            <div className="card">
              <div className="card-body text-center">
                <h3 className="welcomeFont">Welcome to E-ems</h3>
                <h3 className="mb-4">Login</h3>
                {state.isAuthenticated ? (
                  <div>
                    <span>You have already logged to this system</span>
                    <p className="mb-2 text-muted">
                      Back to home page? <NavLink to="/home">HomePage</NavLink>
                    </p>
                    <p className="mb-0 text-muted">
                      Want to log out?{" "}
                      <Link onClick={onClickLogOut}>LogOut</Link>
                    </p>
                  </div>
                ) : (
                  <div>
                    <div className="mb-2">
                      {state.error ? ( 
                      <Alert show={showAlert} onClose={() => setAlert(false)} dismissible variant="danger">
                        {state.errorMessage}
                      </Alert>
                        ) : (null)}
                      <label className="input-group labelForm" name="lblEmail">
                        E-mail
                      </label>
                      <div className="input-group">
                        <input
                          name="username"
                          type="email"
                          className="form-control"
                          placeholder="i.e. : user@mail.com"
                          ref={register({ required: true })}
                        />
                      </div>
                      <span className="error">
                        {errors.username?.type === "required" &&
                          "Email is required"}
                      </span>
                    </div>
                    <div className="mb-2">
                      <label
                        className="input-group labelForm"
                        name="lblPassword"
                      >
                        Password
                      </label>
                      <div className="input-group">
                        <input
                          name="password"
                          type="password"
                          className="form-control"
                          placeholder="Password"
                          ref={register({ required: true })}
                        />
                      </div>
                      <span className="error">
                        {errors.password?.type === "required" &&
                          "Password is required"}
                      </span>
                    </div>
                    <button
                      className="btn btn-primary shadow-2 mb-4"
                      onClick={onButtonClicked}
                    >
                      Login
                    </button>
                    <p className="mb-2 text-muted">
                      Forgot password?{" "}
                      <NavLink to="/auth/forgotpassword">Reset</NavLink>
                    </p>
                    <p className="mb-0 text-muted">
                      Don’t have an account?{" "}
                      <NavLink to="/auth/signup">Signup</NavLink>
                    </p>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </form>
    </main>
  );
}

export default SignIn;
