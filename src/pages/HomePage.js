import React, { useContext, useEffect } from "react";
import { useHistory, NavLink } from "react-router-dom";

import Config from "./../config/Config";
import { AppStore, USER_LOGOUT, SET_ERROR } from "./../stores/AppStore";

const axios = require("axios");

function HomePage() {
  const history = useHistory();
  const { state, dispatch } = useContext(AppStore);

  const onLogOutClick = () => {
    const userDataFromContext = state.userData;
    const requestData = {
      username: userDataFromContext.username,
      accessToken: state.token.accessToken,
    };

    //call API
    var urlLink = Config.apiBasePath + Config.authPath + Config.authPathSignOut;

    axios
      .post(urlLink, requestData)
      .then((response) => {
        //console.log(response);

        const payload = {
          userData: {
            username: "",
            email: "",
            name: "",
            address: "",
            birthDate: "",
            gender: "",
            middleName: "",
          },
          token: { idToken: "", accessToken: "" },
        };

        dispatch({
          type: USER_LOGOUT,
          payload: payload,
        });

        history.push("/");
      })
      .catch((err) => {
        if (err.response) {
          // client received an error response (5xx, 4xx)
          dispatch({
            type: SET_ERROR,
            error: err.response,
          });

          //console.log(err.response);
          var errorStatus = err.response.status;
          var errorMessage = err.response.data.errors[0].message;
          alert("(" + errorStatus + ") " + errorMessage);
        } else if (err.request) {
          // client never received a response, or request never left
          console.log(err.request);
          alert("Error Reqeust API, please see log console for more details.");
        } else {
          // anything else
        }
      });
  };
  const onLogInClick = () => {
    history.push("/");
  };

  useEffect(() => {
    console.log(state);
  }, [state]);

  return (
    <div className="auth-wrapper">
      <div className="auth-content">
        <div className="card">
          <div className="card-body text-center">
            <h3 className="welcomeFont">Welcome to E-ems</h3>
            <h3 className="mb-4">Home Page</h3>
            {state.isAuthenticated ? (
              <div>
                <div>
                  <h4 className="mb-4">User Data</h4>
                  <div>
                    <label className="input-group labelForm">
                      Username: {state.userData.username}
                    </label>
                  </div>
                  <div>
                    <label className="input-group labelForm">
                      Name: {state.userData.name} {state.userData.middleName}
                    </label>
                  </div>
                  <div>
                    <label className="input-group labelForm">
                      Gender: {state.userData.gender}
                    </label>
                  </div>
                </div>
                <div>
                  <NavLink to="/auth/changepassword">Change Password</NavLink>{" "}
                </div>
                <button onClick={onLogOutClick} className="btn btn-primary">
                  Log Out
                </button>
              </div>
            ) : (
              <div>
                <span>You need to login first to access this page</span>
                <button onClick={onLogInClick} className="btn btn-primary">
                  Log In
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default HomePage;
