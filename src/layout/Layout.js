import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import Sidebar from "./Sidebar";
import SignIn from "./../pages/authentication/SignIn";
import SignUp from "./../pages/authentication/SignUp";
import ForgotPassword from "./../pages/authentication/ForgotPassword";
import ConfirmForgotPassword from "./../pages/authentication/ConfirmForgotPassword";
import ChangePassword from "./../pages/authentication/ChangePassword";
import SignUpConfirmation from "./../pages/authentication/SignUpConfirmation";
import HomePage from "./../pages/HomePage";
import Dashboard from "./../pages/eventInformation/Dashboard";
import UserProfile from "./../pages/accountManagement/UserProfile";
import UpdateProfile from "./../pages/accountManagement/UpdateProfile";

export default function Layout() {
  
  return (
    <Router>
      <div className="app ">
        <Sidebar />
        <Switch>
          <Route path="/auth/signup" component={SignUp} />
          <Route path="/auth/forgotpassword" component={ForgotPassword} />
          <Route
            path="/auth/confirmforgotpassword"
            component={ConfirmForgotPassword}
          />
          <Route path="/auth/signin">
            <SignIn />
          </Route>
          <Route
            path="/auth/signupconfirmation"
            component={SignUpConfirmation}
          />
          <Route path="/acct/changepassoword" component={ChangePassword} />
          <Route path="/acct/userprofile">
            <UserProfile />
          </Route>
          <Route path="/acct/UpdateProfile" component={UpdateProfile} />
          <Route path="/dashboard">
            <Dashboard />
          </Route>
          <Route path="/home" component={HomePage} />
          <Route
            exact
            path="/"
            render={() => {
              return <Redirect to="/dashboard" />;
            }}
          />
        </Switch>
      </div>
    </Router>
  );
}