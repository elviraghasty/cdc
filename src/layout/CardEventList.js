import React from "react";

import { Card } from "react-bootstrap";
import moment from "moment";
import { FaCalendarAlt, FaMapMarkerAlt } from "react-icons/fa";

function CardEventList(props) {
  const eventItem = props.children;
  const styles = {
    cardImage: {
      objectFit: "cover",
      height: "210px",
    },
  };

  const formatDate = (date) => {
    let parsed = moment(date);
    let format1 = parsed.format("DD MMM YYYY");
    let format2 = parsed.format("LT");
    const result = format1 + " " + format2;
    return result;
  };

  return (
    <Card style={{ width: "22rem" }} key={eventItem.id} bg="light" text="dark">
      <Card.Img
        data-testid="card-image"
        variant="top"
        src={eventItem.image}
        alt="Card image cap"
        style={styles.cardImage}
        onError={(e) => {
          e.target.onerror = null;
          e.target.src =
            "https://scora.cimsa.or.id/wp-content/themes/glow/images/blank.png";
        }}
      />
      <Card.Body data-testid="card-body">
        <Card.Title data-testid="card-body-title">
          <h5>{eventItem.title}</h5>
        </Card.Title>
        <Card.Text data-testid="card-body-text-datetime">
          <span className="row">
            <span className="col-1">
              <FaCalendarAlt />
            </span>
            <span className="col">
              {formatDate(eventItem.startDate)} to <br></br>
              {formatDate(eventItem.endDate)}
            </span>
          </span>
        </Card.Text>
        <Card.Text data-testid="card-body-text-venue">
          <FaMapMarkerAlt /> {eventItem.venueAddress}
        </Card.Text>
      </Card.Body>
    </Card>
  );
}

export default CardEventList;
