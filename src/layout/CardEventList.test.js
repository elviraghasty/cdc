import React from "react";
import { render } from "@testing-library/react";

import CardEventList from "./CardEventList";

test("CardEventList rendering correctly", () => {
  const eventProps = {
    children: {
      createdBy: null,
      createdDate: null,
      updatedBy: "emapps.mitrais@gmail.com",
      updatedDate: "2020-06-30T05:08:55.401+0000",
      statusId: 1,
      id: "5ef94259a7b11b00016ecd5e",
      name: "Hammersonic",
      categoryName: "Outside event",
      title: "Hammersonic Festival 2020",
      description: "Yearly metal event",
      image:
        "https://mitrais-cdc-team4-emapps.s3-ap-southeast-1.amazonaws.com/C4467086-CC80-4574-B1E4-F64C5D3816D5-4437-00000A589EA9F810.jpeg",
      startDate: "2020-11-04T15:00:00.000+0000",
      endDate: "2020-11-06T15:00:00.000+0000",
      venueAddress: "Ancol",
      location: false,
      userNote: "Meat",
      register: true,
    },
  };

  const { getByTestId } = render(<CardEventList>{eventProps}</CardEventList>);

  const elmCardImage = getByTestId("card-image");
  const elmCardBody = getByTestId("card-body");
  const elmCardBodyTitle = getByTestId("card-body-title");
  const elmCardBodyTextDatetime = getByTestId("card-body-text-datetime");
  const elmCardBodyTextVenue = getByTestId("card-body-text-venue");

  expect(elmCardImage).toBeInTheDocument();
  expect(elmCardBody).toBeInTheDocument();
  expect(elmCardBodyTitle).toBeInTheDocument();
  expect(elmCardBodyTextDatetime).toBeInTheDocument();
  expect(elmCardBodyTextVenue).toBeInTheDocument();
});
