import React from "react";
import { useHistory } from "react-router-dom";

function NeedToLogin() {
  const history = useHistory();

  const onLogInClick = () => {
    history.push("/auth/signin");
  };

  return (
    <div>
      <span className="mb-4">You need to login first to access this page</span>
      <button onClick={onLogInClick} className="btn btn-primary">
        Log In
      </button>
    </div>
  );
}

export default NeedToLogin;
