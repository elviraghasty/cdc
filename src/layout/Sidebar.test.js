import React from "react";
import { render } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";

import Sidebar from "./Sidebar";

test("Sidebar rendering correctly", () => {
  const { getByTestId } = render(
    <Router>
      <Sidebar />
    </Router>
  );

  const elmProSidebar = getByTestId("proSidebar");
  expect(elmProSidebar).toBeInTheDocument();

  const elmProSidebarHeader = getByTestId("proSidebar-header");
  expect(elmProSidebarHeader).toBeInTheDocument();

  const elmProSidebarHeaderEmapps = getByTestId("proSidebar-header-emmaps");
  expect(elmProSidebarHeaderEmapps).toBeInTheDocument();

  const elmProSidebarContent = getByTestId("proSidebar-content");
  expect(elmProSidebarContent).toBeInTheDocument();

  const elmProSidebarContentMenu = getByTestId("proSidebar-content-menu");
  expect(elmProSidebarContentMenu).toBeInTheDocument();

  const elmProSidebarContentSubMenuEvent = getByTestId(
    "proSidebar-content-submenu-event"
  );
  expect(elmProSidebarContentSubMenuEvent).toBeInTheDocument();

  const elmProSidebarContentMenuItemEvent = getByTestId(
    "proSidebar-content-menuitem-events"
  );
  expect(elmProSidebarContentMenuItemEvent).toBeInTheDocument();

  const elmProSidebarContentSubMenuAccount = getByTestId(
    "proSidebar-content-submenu-account"
  );
  expect(elmProSidebarContentSubMenuAccount).toBeInTheDocument();

  const elmProSidebarContentMenuItemUP = getByTestId(
    "proSidebar-content-menuitem-userprofile"
  );
  expect(elmProSidebarContentMenuItemUP).toBeInTheDocument();

  const elmProSidebarContentMenuItemCPass = getByTestId(
    "proSidebar-content-menuitem-changepass"
  );
  expect(elmProSidebarContentMenuItemCPass).toBeInTheDocument();

  const elmProSidebarFooter = getByTestId("proSidebar-footer");
  expect(elmProSidebarFooter).toBeInTheDocument();
});
