import React, { useContext } from "react";
import {
  ProSidebar,
  Menu,
  MenuItem,
  SubMenu,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
} from "react-pro-sidebar";
import { FaStar, FaUserAlt } from "react-icons/fa";
import { Link } from "react-router-dom";
import { AppStore } from "./../stores/AppStore";

const Sidebar = ({ image, collapsed, rtl, toggled, handleToggleSidebar }) => {
  const { state } = useContext(AppStore);

  return (
    <ProSidebar width="350px" data-testid="proSidebar">
      <SidebarHeader data-testid="proSidebar-header">
        <div
          style={{
            padding: "0 24px",
            textTransform: "uppercase",
            fontWeight: "bold",
            fontSize: 14,
            letterSpacing: "1px",
            overflow: "hidden",
            textOverflow: "ellipsis",
            whiteSpace: "nowrap",
          }}
          data-testid="proSidebar-header-emmaps"
        >
          EMAPPS
        </div>
      </SidebarHeader>
      <SidebarContent data-testid="proSidebar-content">
        <Menu data-testid="proSidebar-content-menu">
          <SubMenu
            title="Events"
            open={true}
            icon={<FaStar />}
            data-testid="proSidebar-content-submenu-event"
          >
            <MenuItem data-testid="proSidebar-content-menuitem-events">
              <Link to="/"> Browse Events</Link>
            </MenuItem>
          </SubMenu>

          {state.isAuthenticated ? (
            <SubMenu
              title="Account"
              open={true}
              icon={<FaUserAlt />}
              data-testid="proSidebar-content-submenu-account"
            >
              <MenuItem data-testid="proSidebar-content-menuitem-userprofile">
                <Link to="/acct/userprofile"> User Profile</Link>
              </MenuItem>
              <MenuItem data-testid="proSidebar-content-menuitem-changepass">
                <Link to="/acct/changepassoword"> Change Password</Link>
              </MenuItem>
            </SubMenu>
          ) : (
            <SubMenu
              title="Account"
              open={true}
              icon={<FaUserAlt />}
              data-testid="proSidebar-content-submenu-account"
            >
              <MenuItem data-testid="proSidebar-content-menuitem-userprofile">
                <Link to="/auth/signin"> Log In</Link>
              </MenuItem>
            </SubMenu>
          )}
        </Menu>
      </SidebarContent>
      <SidebarFooter
        style={{ textAlign: "center" }}
        data-testid="proSidebar-footer"
      >
        <div className="sidebar-btn-wrapper">
          <span className="sidebar-text">
            <span> emapps v0.1</span>
          </span>
        </div>
      </SidebarFooter>
    </ProSidebar>
  );
};

export default Sidebar;
